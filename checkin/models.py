from django.db import models
from django.conf import settings
from django.utils.translation import ugettext as _
from django.contrib.auth.models import AbstractUser


class ServiceProvider(models.Model):
    uuid = models.CharField(unique=True)
    BACKEND_CHOICES = (
        ('http', 'HTTP'),
        ('smtp', 'SMTP'),
        ('servo', 'Servo')
    )
    backend_type = models.CharField()
    name = models.CharField(max_length=128)
    http_url = models.URLField()
    http_username = models.CharField(max_length=128)
    http_password = models.CharField(max_length=128)
    smtp_address = models.EmailField()
    servo_address = models.URLField()


class Device(models.Model):
    sn = models.CharField(
        blank=True,
        default='',
        max_length=32,
        verbose_name=_("Serial Number")
    )
    description = models.CharField(
        max_length=128,
        default=_("New Device"),
        verbose_name=_("description")
    )
    WARRANTY_CHOICES = (
        ('ALW', _("Apple Limited Warranty")),
        ('APP', _("AppleCare Protection Plan")),
        ('CSC', _("Customer Satisfaction (CS) Code")),
        ('CBC', _("Custom Bid Contracts")),
        ('WTY', _("3'rd Party Warranty")),
        ('OOW', _("Out Of Warranty (No Coverage)")),
        ('NA', _("Unknown")),
    )

    warranty_status = models.CharField(
        max_length=3,
        default="NA",
        choices=WARRANTY_CHOICES,
        verbose_name=_("Warranty Status")
    )
    username = models.CharField(
        blank=True,
        default='',
        max_length=32,
        verbose_name=_("username")
    )
    password = models.CharField(
        blank=True,
        default='',
        max_length=32,
        verbose_name=_("password")
    )


class User(AbstractUser):
    """docstring for ClassName"""
    street_address = models.CharField(
        null=True,
        blank=True,
        max_length=128,
        verbose_name=_('address')
    )
    zip_code = models.CharField(
        null=True,
        blank=True,
        max_length=32,
        verbose_name=_('ZIP Code')
    )
    city = models.CharField(
        null=True,
        blank=True,
        max_length=32,
        verbose_name=_('city')
    )
