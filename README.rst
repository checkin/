Introduction
============
Servo Checkin is a tool for checking in Apple hardware for service. It's designed for both consumers and customers. Should work on both desktop and mobile devices. The service will be a free public service for consumers to manage their Apple hardware and a paid service for Service Providers who can:
- Link to the service from their homepage
- Use the service at their stores as a self-service system

Business customers should be able to check in their service cases with their prefered service provider as well as receive updates and give feedback on a case.

Features
========
Customers should be able to:
- Create an account and manage their Apple hardware
- Check in a device for service without creating an account
- Check the status of their current service cases (if they've created an account)

Managing hardware
=================
- Customers should be able to check the warranty of their device.
- Upload purchase receipts and other relevant documents. Mark a device as stolen (?).
- Check part prices (with proper service provider backend)
- Check the specs of the machine (what type of RAM, supported OS, etc)
- See current repair extension programs for the device

Creating a Service Case
=======================
- Choose language
- Enter serial number
- Enter problem description
- Enter username and password
- Enter whether data backup is necessary
- Enter maximum price for repair (if OOW)
- Troubleshooting

During service
==============
- Customers should be able to check the status of the repair
- See the latest reported notes by the technician
- See the totall cost of the repair
- Authorize or decline a cost estimate

After service
=============
- Customers should be able to give feedback on the case
- Rate the case (1-5 on speed, professionalism... check CSAT)
- Overall time to repair completion
- Professionalism of the service location employees
- Quality of the repair and condition of the repaired product
- Timeliness of updates provided, especially if the repair is delayed
- Follow-up? (somehow checking that the problem is really gone?)

Troubleshooting
===============
- Help the customer describe the problem
- Help the service provider get the information they need
- Admin-configurable (question: choices)
- Creates a "story"

Backends
========
Servo Checkin should be a separate product from Servo. This would allow:
- providing a public service for customers to manage their Apple hardware
- sell the system separately to service providers who don't need/want Servo

Checkin should have 3 possible backend choices:
- Email (just dump all the entered text into an email)
- HTTP post (JSON, authenticated)
- Servo

The Servo backend
=================
- Associate the customer with a Servo customer with their email address (?)
- Create the Service Order
- Add the customer's notes (by the API user)
- Update the service order with input from the customer duing the repair
- The customer can print the work confirmation
- The Service Provider gets a custom URL (http://checkin.servoapp.com/provider)

The Implementation
==================
- Django app
- PostgreSQL
- south
- Use as many generic class-based views as possible
- Bootstrap 3
- warranty checkup through py-gsxws
- Desktop, phone, tablet

Checkin Admin
=============
- Create AASP accounts (backend, urls, etc)
- Show some customer stats? (no. of logins...)
